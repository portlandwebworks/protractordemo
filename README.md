AngularJS Protractor Demo Project
=================================

## Installation Requirements
- [Install Node JS](http://nodejs.org/download/)
- [Install Yeoman](http://yeoman.io)
- [Install Protractor](https://github.com/angular/protractor)

### Assumptions
- You have Chrome installed on your system
- These instructions work great on a Macbook. Windows users, your mileage may vary

## Running the Test
- Navigate to the top directory of this project (where this file is located) in a terminal
- Load the required libraries for the project

		npm install
and

		bower install

- Start the Selenium server by executing in a terminal

		webdriver-manager start

- Start the example application by opening another terminal and executing

		grunt serve

- Assuming you have installed the required frameworks and libraries, execute

		protractor protractorConf.js

The last command should have caused protractor to open an instance of Chrome and execute the test. It's quick!


## Useful Links
- [Getting started with Protractor](https://github.com/angular/protractor/blob/master/docs/getting-started.md)
- [The Protractor API](https://github.com/angular/protractor/blob/master/docs/api.md)
- [Debugging Protractor](https://github.com/angular/protractor/blob/master/docs/debugging.md)
- [The Jasmine Unit Testing Framework API](http://jasmine.github.io/2.0/introduction.html)
