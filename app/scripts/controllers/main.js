'use strict';

angular.module('protractorDemoApp')
  .controller('MainCtrl', function ($scope, $log) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.addItem = function(){

    	$log.info("addItem was called");

    	if(!$scope.newAwesomeThing)
    	{
    		return;
    	}

    	$scope.awesomeThings.push($scope.newAwesomeThing);
    	$scope.newAwesomeThing = null;


    };
  });
