function  HomeScreenPage(){
}

HomeScreenPage.prototype.getButton = function() {
	return element(by.id('smurfButton'));
};

HomeScreenPage.prototype.getButtonText = function() {
	return this.getButton().getText();
};


module.exports = HomeScreenPage;