var Pages = require('./pages');

var homeScreenPage = new Pages.HomeScreenPage();

describe('awesome list', function() {
	it('should add an item to the list', function(argument) {


		browser.get('http://localhost:9000');

		expect(homeScreenPage.getButtonText()).toMatch('Smurf!');

		element.all(by.repeater('thing in awesomeThings')).then(function(rows) {
			expect(rows.length).toBe(3);
		});

		element(by.model('newAwesomeThing')).sendKeys('I feel awesome!');

		browser.debugger();

		element(by.id('addItemBtn')).click();

		element.all(by.repeater('thing in awesomeThings')).then(function(rows) {
			expect(rows.length).toBe(4);

			expect(rows[rows.length - 1].getText()).toBe('I feel awesome!');

			browser.debugger();
		});

	});
});